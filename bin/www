const http = require('http');
const PORT = 3000;

const allowedMethods = ['GET'];
const allowedPaths = ['/', '/readiness'];
const invalidRequest = (method, url) => `
  <html>
    <body>
      <h1>404: NOT FOUND</h1>
      <p>You requested: ${method} ${url}</p>
      <h2>Allowed Requests:</h2>
      <ul>
        <li>GET /</li>
        <li>GET /readiness</li>
      </ul>
    </body>
  </html>
`;

const server = http.createServer((req, res) => {
  const { method, url } = req;

  if (!allowedPaths.includes(url) || !allowedMethods.includes(method)) {
    res.writeHead(404, { 'Content-Type': 'text/html' });
    res.write(invalidRequest(method, url));
    res.end();
  }

  if (req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify({ message: 'Hola Amigos!' }));
    res.end();
  }

  if (req.url === '/readiness') {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.write(JSON.stringify({ isReady: true }));
    res.end();
  }
});

server.listen(PORT, () => console.info(`NodeJS process ${process.pid} -- listening on port ${PORT}`));

/**
 * Graceful shutdown of node process
 *
 * periodSeconds    -- periodically probe when to force close connections -- default: 1000 (1 second)
 * timeoutThreshold -- number of attemps before forcing connections to close -- default: 2
 * TOTAL TIME to wait for sockets to close = periodSeconds * timeoutThreshold -- default: 2 seconds
 */
const periodSeconds = 1000;
const timeoutThreshold = 2;
let sockets = {};
let nextSocketId = 0;

server.on('connection', socket => {
  const socketId = nextSocketId++;
  sockets[socketId] = socket;

  socket.once('close', () => {
    delete sockets[socketId];
  });
});

function waitForSocketsToClose(timeout) {
  if (timeout > 0) return setTimeout(waitForSocketsToClose, periodSeconds, timeout - 1);

  console.warn(`Forcing ${Object.keys(sockets).length} connections to close now`);
  for (let socketId in sockets) {
    sockets[socketId].destroy();
  }
}

function shutdown() {
  waitForSocketsToClose(timeoutThreshold);

  server.close(err => {
    if (err) {
      console.error(err);
      process.exitCode = 1;
    }

    const endTime = process.hrtime(shutDownInitiatedStartTime);
    console.info(`Shutdown duration: %ds %dms`, endTime[0], endTime[1] / 1000000);
    process.exit();
  });
}

process.on('SIGINT', () => {
  console.info(`SIGINT (aka ctrl-c in docker) @ ${new Date().toISOString()}`);
  console.log(`Closing ${Object.keys(sockets).length} connections`);
  console.log(`Total wait time before FORCING connections to close: ${(timeoutThreshold * periodSeconds) / 1000} seconds`);

  shutDownInitiatedStartTime = process.hrtime();
  shutdown();
});

process.on('SIGTERM', () => {
  console.info(`SIGTERM (docker container stop) @ ${new Date().toISOString()}`);
  console.log(`Closing ${Object.keys(sockets).length} connections`);
  console.log(`Total wait time before FORCING connections to close: ${(timeoutThreshold * periodSeconds) / 1000} seconds`);

  shutDownInitiatedStartTime = process.hrtime();
  shutdown();
});
